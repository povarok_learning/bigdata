package data_aggregation
import java.io.{BufferedWriter, File, FileWriter}
import scala.util.control.Breaks.break
import com.google.gson.Gson
import scala.io.Source

// Комаров 20 вариант id=553
object Main {
  def main(args: Array[String]): Unit = {
    var data: List[(Int, Int)] = List()
    Source.fromResource("ml-100k/u.data").getLines().foreach(row => {
      val rowData = row.split('\t')
      if (rowData.length == 4) {
        data = data :+ ((rowData(1).toInt, rowData(2).toInt))
      }
    })

    val film_map = data.filter(_._1.equals(553)).groupBy(_._2).mapValues{f => f.length}.toMap
    val all_map = data.groupBy(_._2).mapValues{f => f.length}.toMap
    save(film_map, all_map)
  }

  def save(film_map: Map[Int, Int], all_map: Map[Int, Int]) = {
    val hist_film = getResultList(film_map)
    val hist_all = getResultList(all_map)
    val json_result = JsonResult(hist_film = hist_film, hist_all = hist_all)
    write(json_result)
  }

  def write(res: JsonResult) = {
    val json_string = new Gson().toJson(res)
    val writer = new BufferedWriter(new FileWriter(new File("out.json")))
    writer.write(json_string)
    writer.close()
    println("Complete")
  }

  def getResultList(map: Map[Int, Int]): Array[Int] = {
    var res: Array[Int] = Array()
    val marks = Array(1, 2, 3, 4, 5)
    marks.foreach(mark => {
      val markCount = if (map.contains(mark)) map(mark) else 0
      res = res :+  markCount
    })
    res
  }
}
